const gulp        = require('gulp');
const browserSync = require('browser-sync').create();
const reload      = browserSync.reload;

// Servidor
gulp.task('server', () => {

    browserSync.init({
        server: "./src"
    });

    gulp.watch("src/css/*.css").on('change', reload);
    gulp.watch("src/*.html").on('change', reload);
});

gulp.task('default', ['server']);
